# Desafio DIO

# Criando um sistema bancário
# - Apenas um usuario
# - Operação de deposito
#   - Somente valores positivos
#   - Todos os depositos devem aparecer em extrato
# - Operação de saque
#   - Apenas 3 saques por dia
#   - limite de 500 por saque
#   - Deve alertar em caso de saldo insuficiente
#   - Todos os saques devem aparecer em extrato
# - Operação de extrato
#   - Listar todas as operações saque/deposito
#   - No final da listagem, exibir saldo atual
#       -   Os valores devem ser exibidos utilizando formato R$ XXX.XX

import datetime
import os

quantidade_saques = 0;
dic_extrato = []

# Criação de menu de opções
def menu(aviso):
    # os.system('clear')

    print("""

###################### Bem vindo ao banco DIO ###################
#                                                               #
#   Informe a operação que deseja realizar:                     #
#   1 - Extrato                                                 #
#   2 - Saque                                                   #
#   3 - Deposito                                                #
#   4 - Sair                                                    #
#                                                               #
#################################################################
""")
    print(aviso)
    # MENU
    while True:
        try:
            operacao = int(input("\n>> "))
            if operacao not in [1, 2, 3, 4]:
                menu("Opção invalida, selecione uma das opções do menu!", operacao)

            if operacao == 1:
                op_extrato()
            elif operacao == 2:
                op_saque()
            elif operacao == 3:
                op_deposito()
            else:
                exit()
        except ValueError:
            menu("Opção invalida, selecione uma das opções do menu!", operacao)

# Calcula o saldo em conta
def saldo_conta():
    saldo = 0;
    for extrato in dic_extrato:
        if extrato['op'] == "Depósito":
            saldo += extrato['valor']
        else:
            saldo -= extrato['valor']

    return saldo

# Retorna a quantidades de saques realizadas no dia
def total_saques_no_dia():
    total = 0;
    hoje = str(datetime.datetime.now().strftime("%d-%m-%Y"))
    for extrato in dic_extrato:
        datasaque = str(extrato['data']).split(" ")[0]
        if hoje == datasaque:
            if extrato['op'] == "Saque":
                total += 1
    return total

# Realiza um deposito
def op_deposito():
    try:
        valor = float(input("Você Selecionou a Opção de DEPÓSITO\nQual valor deseja depositar R$:"))
    except ValueError:
        menu("Valor invalido! Tente novamente.")

    if valor < 1:
        menu("Valores negativos não são permitidos!")
        return

    x = datetime.datetime.now();
    deposito = {"op": "Depósito", "valor": valor, "data": x.strftime("%d-%m-%Y %H:%M:%S")}
    dic_extrato.append(deposito)
    menu("Deposito realizado com sucesso!")

# Imprime o extrato de operações e saldo em conta
def op_extrato():
    dados = "Você Selecionou a Opção de EXTRATO\n\n"
    dados += "---------------------------------------\n"
    dados += "Extrato:\n"
    saldo = 0;
    for extrato in dic_extrato:

        if extrato['op'] == "Depósito":
            saldo += extrato['valor']
        else:
            saldo -= extrato['valor']

        dados += """
Operação: {op}
Realizada em: {data}
Valor: R${valor:.2f}
""".format(**extrato)
    dados += "---------------------------------------\n"
    dados += f"Seu saldo atual é de R$ {saldo:.2f}\n"
    menu(dados)

# Realiza saque da conta
def op_saque():
    try:
        valor = float(input("Você Selecionou a Opção de SAQUE\nQual valor deseja sacar R$:"))

        if total_saques_no_dia() >= 3:
            menu("Saque não autorizado! Limite de 3 saques por dia atingido.")
            return

        if valor < 1:
            menu("Valores negativos não são permitidos!")
            return

        if valor > 500:
            menu("Saque não autorizado! Máximo de R$500 por saque")
            return

        if valor > saldo_conta():
            menu("Saque não autorizado! Saldo insuficiente.")
            return

        x = datetime.datetime.now();
        deposito = {"op": "Saque", "valor": valor, "data": x.strftime("%d-%m-%Y %H:%M:%S")}
        dic_extrato.append(deposito)
        menu("Saque realizado com sucesso!")
    except ValueError:
        menu("Valor invalido! Tente novamente.")


# Start
menu("")
